//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require turbolinks
//= require bootstrap-datepicker
//= require jquery.validate
//= require jquery.validate.additional-methods
//= require jquery-validate.bootstrap-tooltip.min
//= require dataTables/jquery.dataTables
//= require ckeditor/init

$(document).on("page:fetch", function(){
	$('#big_container').fadeOut();
	$('#page_loader').fadeIn();
	//$('.intro_wrapper .container').hide();
});
$(document).on("page:restore", function(){
	$('#big_container').fadeIn();
	$('#page_loader').fadeOut();
	//$('.intro_wrapper .container').hide();
});