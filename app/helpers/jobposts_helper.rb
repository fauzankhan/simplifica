module JobpostsHelper
	
	def search_applicants(search, jobpost)
		keywords = search.split
		students = Array.new
		keywords.each do |keyword|
			keyword = keyword.downcase
			student_ids = Student.where("lower(first_name) = ? OR lower(last_name) = ? OR email = ?", keyword, keyword, keyword).ids
			students.push(student_ids).flatten!
		end
		jobpost.job_applications.where(student_id: students)
	end

	def view_applicants
		@jobpost = Jobpost.find(params[:id])
		@show_selected = params[:show_selected]
		@show_shortlisted = params[:show_shortlisted]
		@search = params[:search_applicant]
		if params[:search_applicant]
			#students = Student.search_applicants(params[:search], params[:id])
			@applications = search_applicants(params[:search_applicant], @jobpost) 
		elsif @show_selected
			@applications = @jobpost.job_applications.where(selected: true)
		elsif @show_shortlisted
			@applications = @jobpost.job_applications.where(shortlisted: 1)
		else
			@applications = @jobpost.job_applications
		end
		@applications = [] if @applications.nil?
		#@applications = @applications.page(params[:page]).per(50)
	end

	def print_applicants
		@jobpost = Jobpost.find(params[:id])
		if params[:selected]
			@applications = @jobpost.job_applications.where(selected: true)
		elsif params[:shortlisted]
			@applications = @jobpost.job_applications.where(shortlisted: 1)
		else
			@applications = @jobpost.job_applications
		end
		@applications = [] if @applications.nil?
	end

	def download_excelsheet
		@jobpost = Jobpost.find(params[:id])
		if params[:selected]
			@applications = @jobpost.job_applications.where(selected: true)
		elsif params[:shortlisted]
			@applications = @jobpost.job_applications.where(shortlisted: 1)
		else
			@applications = @jobpost.job_applications
		end
		@applications = [] if @applications.nil?
		@applicant_ids = @applications.pluck(:student_id)
		respond_to do |format| 
			if params[:selected]
	       		format.xlsx {render xlsx: 'download_excelsheet', filename: "selected_"+@jobpost.company+"_"+@jobpost.position+"_applicants"}
	       	elsif params[:shortlisted]
	       		format.xlsx {render xlsx: 'download_excelsheet', filename: "shortlisted_"+@jobpost.company+"_"+@jobpost.position+"_applicants"}
	    	else
	    		format.xlsx {render xlsx: 'download_excelsheet', filename: "all_"+@jobpost.company+"_"+@jobpost.position+"_applicants"}
	    	end
	       		
	    end
	end

	def already_applied?
		jobpost = Jobpost.find(params[:id])
		student = current_user
		applied = JobApplication.find_by(student_id: student.id, jobpost_id: jobpost.id)
		!applied.nil?
	end

	def can_apply?
		jobpost = Jobpost.find(params[:id])
		author = Tpo.find(jobpost.tpo_id)
		student = current_user
		if jobpost.job_type == 1 || jobpost.job_type == 4
			student.college_id == author.college_id
		else
			return true
		end
	end

	def job_expired?
		jobpost = Jobpost.find(params[:id])
		Date.today > jobpost.last_date
	end

	def is_eligible?
		student = current_user
		student_course = student.course_id
		student_branch = student.branch_id
		student_percentage = student.percentage
		student_batch = student.year_of_passing
		j = Jobpost.find(params[:id])
		eligible_courses = j.course_ids
		eligible_branches = j.branch_ids
		eligible_percentage = j.percentage_required
		eligible_batch = j.year_required
		student_percentage >= eligible_percentage && eligible_courses.include?(student_course) && eligible_branches.include?(student_branch) && student_batch == eligible_batch
	end

	def shortlist_applicant
		applicant_id = params[:applicant_id]
		jobpost_id = params[:jobpost_id]
		@jobpost = Jobpost.find(jobpost_id)
		job_application = JobApplication.find_by(student_id: applicant_id, jobpost_id: jobpost_id)
		shortlisted_for_round = job_application.shortlisted
		shortlisted_for_round += 1
		respond_to do |format|
			if job_application.update_attribute(:shortlisted, shortlisted_for_round)
				@jobpost.send_application_status_mailer(applicant_id, "shortlisted")
				format.html { render :nothing => true, :notice => 'Update SUCCESSFUL!' } 
			end
		end
	end

	def select_applicant
		applicant_id = params[:applicant_id]
		jobpost_id = params[:jobpost_id]
		@jobpost = Jobpost.find(jobpost_id)
		job_application = JobApplication.find_by(student_id: applicant_id, jobpost_id: jobpost_id)
		respond_to do |format|
			if job_application.update_attribute(:selected, true)
				job_application.update_attribute(:shortlisted, 1)
				@jobpost.send_application_status_mailer(applicant_id, "selected")
				format.html { render :nothing => true, :notice => 'Update SUCCESSFUL!' } 
			end
		end
	end

	def tpo_note
		all = params[:all]
		shortlisted = params[:shortlisted]
		selected = params[:selected]
		jobpost = Jobpost.find(params[:id])
		jobpost.update(params.require(:jobpost).permit(:notification_mailer))
		if all
			applications = jobpost.job_applications
		elsif shortlisted && selected
			applications = jobpost.job_applications.where("shortlisted = 1 OR selected = true")
		elsif shortlisted
			applications = jobpost.job_applications.where(shortlisted: 1)
		elsif selected
			applications = jobpost.job_applications.where(selected: 1)
		else
			flash[:danger] = "Please select recipients for your message"
			redirect_to :back
		end
		applicants=Array.new
		applications.each do |a|
			applicant = Student.find(a.student_id)
			applicants.push(applicant)
		end
		jobpost.send_tpo_note_mailer(applicants)
		flash[:success] = "Mail sent to #{applications.count} applicant(s)"
		redirect_to :back
	end
	
end
