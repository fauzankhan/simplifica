module TposHelper

	def admin_panel

	end

	def search_students
		keyword = params[:search].strip.gsub(/\s+/, " ")
		college_id = current_user.college_id
		@students = Student.search(keyword, college_id)
	end

	def update_student_colleges(college_id)
		other_college_id = College.find_by(college_name: 'Other')
		effected_students = Student.where(college_id: college_id)
		effected_students.each do |e|
			e.update_attribute(:college_id, other_college_id)
		end
	end

	def check_college_name_availability
		college_name = params[:college].downcase.strip.gsub(/\s+/, " ")
		college = College.find_by(college_name: college_name.titleize)
		college = nil if !current_user.nil? &&  !college.nil? && college.id == current_user.college_id
		respond_to do |format| 
			format.json { render :json => college.nil? }
		end
	end

end
