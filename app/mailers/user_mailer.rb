class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.account_activation.subject
  #
  def account_activation(user)
    @user = user
    @greeting = "Hi"

    mail to: user.email, subject: "Account activation"
  end

  def new_college_registered(tpo)
    @tpo = tpo
    mail to: 'fauzankhan93@yahoo.in', subject: "New Tpo Sign Up"
  end

  def tpo_welcome_message(tpo)
    @tpo = tpo
    mail to: @tpo.email, subject: "Thank you for signing up with us"
  end

  def tpo_activation(tpo)
    @tpo = tpo
    mail to: @tpo.email, subject: "Simplifica Account Activation"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    @user = user
    @greeting = "Hi"

    mail to: user.email, subject: "Password Reset"
  end
end
