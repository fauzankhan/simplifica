class ApplicationMailer < ActionMailer::Base
  default from: "support@simplifica.co.in"
  layout 'mailer'
end
