class TposController < ApplicationController

	before_action :logged_in_user, only: [:edit, :update, :show, :destroy, :index, :admin_panel]
	before_action :correct_user, only: [:edit, :update]
	before_action :admin_user, only: [:destroy, :index, :activate_tpo, :admin_panel]
	before_action :redirect_if_logged_in, only: [:new, :create]

	include TposHelper

	def new
		@tpo = Tpo.new
	end

	def index
		@tpos = Tpo.all - Tpo.where(admin: true)
	end

	def show
		@tpo = Tpo.find(params[:id])
		if current_user.college_id == @tpo.college_id
			@jobposts = @tpo.jobposts.page(params[:page]).per(50)
		else
			@jobposts = @tpo.jobposts.where("job_type = ? OR job_type = ?", 2, 3).page(params[:page]).per(50)
		end
	end

	def update
		@tpo = Tpo.find(params[:id])
		if @tpo.update_attributes(update_tpo_params)
			@tpo.update_collge
			flash[:success] = "Profile successfully updated"
      		redirect_to @tpo
		else
			render 'edit'
		end
	end

	def create
		@tpo = Tpo.new(tpo_params)
		@tpo.first_name = params[:tpo][:first_name].titleize
		@tpo.last_name = params[:tpo][:last_name].titleize
		college = params[:tpo][:college].titleize
		location_id = params[:tpo][:location_id]
		if @tpo.save && @tpo.create_college(college, location_id)
        	@tpo.update_column(:college_id, College.find_by(college_name: college).id)
			@tpo.send_mail_to_site_admin
			@tpo.send_welcome_email
			flash[:info] = "Thank you for signing up with us. We'll Contact you soon to verify your account"
			redirect_to root_url
		else
			render 'new'
		end
	end

	def edit
		@tpo = Tpo.find(params[:id])
	end

	def destroy
		tpo = Tpo.find(params[:id])
		college_id = tpo.college_id
		college = College.find(college_id)
		if update_student_colleges(college_id) && college.destroy && tpo.destroy
			flash[:success] = "Tpo & corresponding college deleted"
			redirect_to :back
		end
	end

	def activate_tpo
		tpo_id = params[:tpo_id]
		tpo = Tpo.find(tpo_id)
		tpo.update_attributes(activated: true)
		tpo.send_tpo_activation_email
		flash[:success] = tpo.college+" successfully activated!"
		redirect_to :back
	end
	
	private

		def tpo_params
			params.require(:tpo).permit(:email, :password, :first_name, :last_name, :location_id, :college, :phone_no)
		end

		def update_tpo_params
			params.require(:tpo).permit(:password, :first_name, :last_name, :job_title, :image, :phone_no, :college, :location_id)
		end

		def correct_user
			@user = Tpo.find(params[:id])
			if !(@user == current_user)
				redirect_to root_path
				flash[:danger] = "Action not allowed as you're not the owner of requested profile"
			end
		end

end
