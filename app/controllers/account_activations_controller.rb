class AccountActivationsController < ApplicationController
	
  def edit
    user = Tpo.find_by(email: params[:email]) || Student.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.activate
      log_in user
      if user.user_type == 2
        flash[:success] = "Account activated! Please Take 2 minutes to complete your profile as your profile works as your CV on simplifiaca"
        redirect_to edit_student_path(user)
      else
        flash[:success] = "Account activated!" 
        redirect_to user
      end
    else
      flash[:danger] = "Invalid activation link"
      redirect_to root_url
    end
  end

  def new

  end

  def create
    user_email = params[:account_activation][:email]
    user = AllUser.find_by(email: user_email)
    if user
      if user.user_type == 1
        flash[:danger] = "Tpos cannot activate their account. If your account is not yet activated, we'll contact you soon"
      elsif user.user_type == 2
        student = Student.find_by(email: user_email)
        if student.activated
          flash[:danger] = "Account already acticvated"
        else
          student.update_activation_digest
          flash[:info] = "We've resent you the account activation instructions. Please check your mail"
        end
      end
      redirect_to root_path
    else
      flash[:danger] = "Email not registered with us. Try entering your email again"
      render 'new'
    end
  end

end
