class Jobpost < ActiveRecord::Base

	default_scope -> { order(created_at: :desc) }

	belongs_to :tpo

	has_many :eligible_courses, dependent: :destroy
	has_many :courses, through: :eligible_courses

	has_many :eligible_branches, dependent: :destroy
	has_many :branches, through: :eligible_branches

	has_many :job_applications, dependent: :destroy
	has_many :students, through: :job_applications 

	validates :tpo_id, presence: true, numericality: true

	validates :location_id, presence: true, numericality: true

	validates :company, presence: true

	validates :position, presence: true

	validates :percentage_required, presence: true, numericality: {less_than_or_equal_to: 100}

	#validates :eligible_courses, presence: true

	validates :venue, presence: true

	validates :last_date, presence: true

	validates :drive_date, presence: true

	validates :year_required, presence: true, numericality: true

	validates :reporting_time, presence: true

	validates_presence_of :courses

	def send_jobpost_mailer
		JobpostUpdate.new_job_posted(self).deliver_now
	end

	def send_application_status_mailer(student_id, status)
		JobpostUpdate.application_update(self, student_id, status).deliver_now
	end

	def send_tpo_note_mailer(applicants)
		JobpostUpdate.tpo_note(self, applicants).deliver_now
	end	

	def self.search(search)
	  where("company iLIKE ?", "%#{search}%") |
	  where("position iLIKE ?", "%#{search}%") |
	  where("venue iLIKE ?", "%#{search}%") |
	  where("job_profile iLIKE ?", "%#{search}%") |
	  where("other_requirements iLIKE ?", "%#{search}%") |
	  where("company || ' ' || position iLIKE?", "%#{search}%") |
	  where("position || ' ' || company iLIKE?", "%#{search}%") 
	end

end
