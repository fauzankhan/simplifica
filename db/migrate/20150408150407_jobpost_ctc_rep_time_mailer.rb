class JobpostCtcRepTimeMailer < ActiveRecord::Migration
  def change
  	add_column :jobposts, :ctc, :string
  	add_column :jobposts, :notification_mailer, :text
  	add_column :jobposts, :reporting_time, :string
  end
end
