class AddLocationIds < ActiveRecord::Migration
  def change
  	add_column :tpos, :location_id, :integer
  	add_column :students, :location_id, :integer#, 'integer USING CAST(location_id AS integer)' #FIX FOR PG DBMS
  	remove_column :colleges, :location
  	add_column :colleges, :location_id, :integer
  	#change_column :colleges, :location_id, 'integer USING CAST(location_id AS integer)' #FIX FOR PG DBMS
  end
end
