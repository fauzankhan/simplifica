jQuery.validator.addMethod("greaterThan", 
function(value, element, params) {

    if (!/Invalid|NaN/.test(new Date(value))) {
        return new Date(value) > new Date($(params).val());
    }

    return isNaN(value) && isNaN($(params).val()) 
        || (Number(value) > Number($(params).val())); 
},'Must be greater than {0}.');

$('.event_info input.date').datepicker({
    format: "D, dd M yyyy",
    startDate: "today",
    orientation: "top right",
    calendarWeeks: true,
    todayHighlight: true
});

$('#select_all_courses_container').on('change', 'input[type="checkbox"]', function(){
    if($(this).is(':checked')){
        $('.courses_selection input[type="checkbox"]').each(function(){
          this.checked = false;
          $(this).trigger('change');
          this.checked = true;
          $(this).trigger('change');
        });
    }
    else{
        $('.courses_selection input[type="checkbox"]').each(function(){
          this.checked = false;
          $(this).trigger('change');  
        });
    }
});

$('#select_all_branches_container').on('change', 'input[type="checkbox"]', function(){
    if($(this).is(':checked')){
        $('.branches_selection .dynamic_branches_container input[type="checkbox"]').each(function(){
          this.checked = true;
        });
    }
    else{
        $('.branches_selection .dynamic_branches_container input[type="checkbox"]').each(function(){
          this.checked = false;
        });
    }
});

$('.courses_selection').on('change', 'input[type="checkbox"]', function(){
    select_all_courses();
    if($(this).is(':checked')){
        $.ajax({
            url: 'populate_branches',
            data: {course_id: $(this).val()},
            beforeSend : function(){
                $('.branches_overlay').show();
            },
            success: function(data, text){
                $('#select_all_branches_container').css('display', 'inline-block');
                $('.branches_selection .message').hide();
                $('#select_all_branches').prop('checked', false);
                $('.branches_overlay').hide();
            },
            error: function(xhr, textStatus, errorThrown){
                alert(textStatus+"-"+errorThrown);
                $('.branches_overlay').hide();
            }

        })
    }
    else{
        var course_id = $(this).val();
        $('.branches_selection input[type="checkbox"]').each(function(){
            if($(this).attr('data-course') == course_id){
                $(this).closest('.branches').remove();
                if($('.dynamic_branches_container').html().trim() == ""){
                    $('.branches_selection .message').show();
                    $('#select_all_branches_container').css('display', 'none');
                }
            }
        });
        $('#select_all_courses').attr('checked', false);
    }
    
});

$('.branches_selection .dynamic_branches_container').on('change', 'input[type="checkbox"]', function(){
    if($(this).is(':checked')){
        select_all_branches();
    }
    else{
        $('#select_all_branches').attr('checked', false);
    }
    
});

function validate_job_form(){
    $('#new_jobpost, .edit_jobpost').validate({

        errorClass : 'invalid_input_field',

        rules : {
                    'jobpost[company]' : {
                                            required : true,
                                            maxlength : 50
                                        },

                    'jobpost[position]' : {
                                            required : true,
                                            maxlength : 50
                                           },

                    'jobpost[percentage_required]' : {
                                                        required : true,
                                                        number : true,
                                                        range : [30, 100]
                                                      },

                    'jobpost[year_required]' : {
                                                    required : true,
                                                    digits : true,
                                                    range : [2014, 2030]
                                                },

                    'jobpost[job_type]' : {
                                            required : true,
                                            digits : true,
                                          },

                    'jobpost[venue]' : {required : true},

                    'jobpost[drive_date]' : {
                                                required : true,
                                                date : true
                                            },

                    'jobpost[last_date]' : {
                                                required : true,
                                                date : true
                                            },

                    'jobpost[location_id]' : {
                                                required : true,
                                                digits : true
                                              },

                    'jobpost[reporting_time]' : {
                                                    required : true
                                                }
                },

                messages : {
                                'jobpost[year_required]' : "Select the batch for which the placement drive is being held.",

                                'jobpost[job_type]' : "Select the type of event from the list.",

                                'jobpost[venue]' : "Enter full address of the venue.",

                                'jobpost[drive_date]' : "Enter a valid date",

                                'jobpost[last_date]' : "Enter a valid date",

                                'jobpost[course_ids][]' : "Select atleast one of the courses"

                                //'jobpost[branch_ids][]' : "Select atleast one of the branches"
                            },

            tooltip_options: {
                                '_all_': {trigger:'focus'},

                                'jobpost[percentage_required]' : {placement : 'top'},

                                'jobpost[job_type]' : {placement : 'right'},

                                'jobpost[venue]' : {placement : 'left'},

                                'jobpost[drive_date]' : {placement : 'right'},

                                'jobpost[last_date]' : {placement : 'left'},

                                'jobpost[location_id]' : {placement : 'left'},

                                'jobpost[course_ids][]' : {placement : 'top'},

                                'jobpost[reporting_time]' : {placement : 'right'}
                             },

            invalidHandler: function(form, validator) {
                $('#jobpost_error_message').removeClass('hidden');
                validate_branches();
            },

            submitHandler: function(form, validator){
                if(validate_branches()){
                    $(form).ajaxSubmit();
                }
            }

    });

    $("#jobpost_drive_date").rules('add', { 
        greaterThan: "#jobpost_last_date", 
        messages: {
            greaterThan : "Drive date should be greater than Last date"
          }
    });

    $('.courses_selection input[type="checkBox"]').rules("add", { 
      required : true,
      minlength : 1,
    });
}

function validate_branches(){
    /*var course_ids = [];
    var i = 0;
    $('.dynamic_branches_container .branches').each(function(){
        course_ids[i] = $(this).find('input[type="checkbox"]').attr('data-course');
        i++;
    });
    for(j = 0; j<course_ids.length; j++){ 
        $('.dynamic_branches_container .branches').find('input[data-course="'+course_ids[j]+'"]').rules("add", { 
                required:true,
                minlength:1
        });
    }*/
    $('.dynamic_branches_container .branches').each(function(){
        var l = $(this).find('input[type="checkbox"]:checked').length
        if(l == 0){
            $(document).scrollTop(0);
            $('#branches_error').removeClass('hidden');
            $('#jobpost_error_message').removeClass('hidden');
            return false;
        }
        else{
             $('#branches_error').addClass('hidden');
        }
    });
    return true;
}

function select_all_courses(){
    if ($('.courses_selection input[type="checkbox"]:checked').length == $('.courses_selection input[type="checkbox"]').length) {
        $('#select_all_courses').prop('checked', true);
    }
}

function select_all_branches(){
    if ($('.dynamic_branches_container input[type="checkbox"]:checked').length == $('.dynamic_branches_container input[type="checkbox"]').length) {
        $('#select_all_branches').prop('checked', true);
    }
}

$('[data-toggle="tooltip"]').tooltip();

if(!($('.dynamic_branches_container').is(':empty'))){
    $('#select_all_branches_container').css('display', 'inline-block');
}

select_all_courses();
select_all_branches();
validate_job_form();
