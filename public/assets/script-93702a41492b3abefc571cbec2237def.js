var ready;

ready = function(){
	
		var window_height = $(window).height();
		var window_width = $(window).width();
		
		function doc_width_adjust(){
			if(window_height > 500 && window_width > 1000){
				$('.window_span').css('height', window_height+'px');//.css('width', window_width+'px');
				$('.intro_wrapper .container').css('height', window_height+'px');
			}
			else{
				$('.window_span').css('height', 900+'px');//.css('width', window_width+'px');
				$('.intro_wrapper .container').css('height', 900+'px');
			}

			if($('.form_container').height() >( ($('.window_span').height())-50)){
				$('.window_span').css('height', 1000+'px');//.css('width', window_width+'px');
				$('.intro_wrapper .container').css('height', 1000+'px');
			}
		}
		doc_width_adjust();
		
		$(window).resize(function(){
			window_height = $(window).height();
			window_width = $(window).width();
			doc_width_adjust()
		});

		$(document).scroll(function(){
			var scroll_top = $(document).scrollTop();
			if(scroll_top > 50)
				$('nav.navbar-fixed-top').css('background', 'rgba(0,0,0,0.6)');
			else
				$('nav.navbar-fixed-top').css('background', '');
		});

		//smooth scroll
		$(function() {
		  $('a[href*=#]:not([href=#])').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html,body').animate({
		          scrollTop: target.offset().top
		        }, 1000);
		        return false;
		      }
		    }
		  });
		});
};
$(document).ready(ready)

$(document).on('page:load', ready);

$(document).on("page:fetch", function(){
	$('.intro_wrapper .container .animated').removeClass('fadeIn');
	$('.intro_wrapper .container .animated').addClass('zoomOut');
	$('#page_loader').fadeIn(1100);
	//$('.intro_wrapper .container').hide();
});
$(document).on("page:restore", function(){
	$('.intro_wrapper .container .animated').addClass('fadeIn');
	$('.intro_wrapper .container .animated').removeClass('zoomOut');
	$('#page_loader').hide();
	//$('.intro_wrapper .container').hide();
});
