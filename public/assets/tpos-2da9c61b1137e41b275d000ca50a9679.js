function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('.user_pic').css('background-image', 'url('+e.target.result+')');
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}

$.validator.addMethod("email", function(value, element){
	return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value); 
}, 
"Please enter a valid email address."
);

$('.edit_tpo').validate({
	
	errorClass: 'invalid_input_field',

	rules: {   
		'tpo[email]' : {
							required: true, 
							email: true,
							remote: {
					        	url: 'check_email_availability',
					        	//type: "get",
					        	data: {
						         	email: function() {
						            	return $('#tpo_email').val();
						          	}
						        }
						    }
						},

		'tpo[first_name]' : {
								required: true, 
								minlength: 3
							},

		'tpo[college]' : {
							required: true,
							remote: {
					        	url: 'check_college_name_availability',
					        	//type: "get",
					        	data: {
						         	college: function() {
						            	return $( "#tpo_college" ).val();
						          	}
						        }
						    }
						  },

		'tpo[location_id]' : {
								required: true,
								digits: true
							},

		'tpo[password]' : {
							minlength: 6,
							maxlength: 12
						  },

		'tpo[phone_no]' : {
							digits: true,
							rangelength: [8,10]
						  }
	},
	messages: {
		'tpo[email]' : {remote: "Email ID already taken."},

		'tpo[college]' : {remote: "College name already taken. Try entering full name of your college with location"}
	},

	tooltip_options: {
		
		'tpo[email]': {placement: 'right',html: true},

		'tpo[first_name]': {placement: 'left',html: true},

		'tpo[college]': {placement: 'left',html: true},

		'tpo[password]': {placement: 'right',html: true},

		'tpo[phone_no]': {placement: 'right',html: true}

	},

	invalidHandler: function(form, validator) {
		$(document).scrollTop(0);
	}
});

$("#tpo_image").change(function(){
    readURL(this);
});
