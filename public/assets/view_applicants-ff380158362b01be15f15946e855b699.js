function shortlist_student(student_id, jobpost_id, link){
    $.ajax({
        url: '/jobposts/shortlist_applicant',
        data: {applicant_id: student_id,
               jobpost_id: jobpost_id},
        beforeSend: function(){
            $(link).siblings('.ajax_overlay').show();
        },
        success: function(data, text){
            $(link).siblings('.ajax_overlay').hide();
            $(link).removeClass('undecided');
            $(link).attr('data-original-title', 'shortlisted');
        },
        error: function(xhr, textStatus, errorThrown){
            $(link).siblings('.ajax_overlay').hide();
            alert(+textStatus+"-"+errorThrown)
        }
    })
}

function select_student(student_id, jobpost_id, link){
    $.ajax({
        url: '/jobposts/select_applicant',
        data: {applicant_id: student_id,
               jobpost_id: jobpost_id},
        beforeSend: function(){
            $(link).siblings('.ajax_overlay').show();
        },
        success: function(data, text){
            $(link).siblings('.ajax_overlay').hide();
            $(link).removeClass('undecided');
            $(link).attr('data-original-title', 'selected');
            $(link).prev().removeClass('undecided');
            $(link).prev().attr('data-original-title', 'selected');
        },
        error: function(xhr, textStatus, errorThrown){
            $(link).siblings('.ajax_overlay').hide();
            alert(+textStatus+"-"+errorThrown)
        }
    })
}

function validate_applicants(){
    var recepient_status = $('.applicants_selection').find('input[type="checkbox"]').is(':checked');
    if(recepient_status)
        return true;
    else
        return false;
}

$('.application_table_container table tr').on('click','a.shortlist.undecided', function(){
    var link = $(this);
    var student_id = $(this).closest('tr.student_info').attr('id');
    var jobpost_id = $(this).closest('table.application_table').attr('data-jobpost-id');
    shortlist_student(student_id, jobpost_id, link)
});

$('.application_table_container table tr').on('click','a.select.undecided', function(){
    var link = $(this);
    var student_id = $(this).closest('tr.student_info').attr('id');
    var jobpost_id = $(this).closest('table.application_table').attr('data-jobpost-id');
    select_student(student_id, jobpost_id, link)
});

$('[data-toggle="tooltip"]').tooltip();

$('.edit_jobpost').validate({

    errorClass : 'invalid_input_field',

    rules : {
                'jobpost[notification_mailer]' : {
                                        required : true
                                    },

                
            },

            messages : {
                            'jobpost[notification_mailer]' : "Please enter your message"
                        },

        tooltip_options: {
                            '_all_': {trigger:'focus'},

                            'jobpost[notification_mailer]' : {placement : 'right'}

                         },

        invalidHandler: function(form, validator) {

        },

        submitHandler: function(form, validator){
            if(validate_applicants()){
                $(form).ajaxSubmit();
            }
            else{
                alert("Please select the recepients");
            }
        }

});

$('#send_note_btn').on('click', function(){
    var form_visible = $('.search_applicants .note_form').is(':visible');
    if(form_visible)
        $('.search_applicants .note_form').slideUp();
    else
        $('.search_applicants .note_form').slideDown();
})

$('#cancel_note_btn').on('click', function(){
    $('.search_applicants .note_form').slideUp();
})

$('#jobpost_applicants').dataTable({
    pagingType: 'simple' 
});
