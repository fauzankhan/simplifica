$(document).on('ready page:change',function(){
	
	$.validator.addMethod("email", function(value, element){
		return this.optional(element) || /^[a-zA-Z0-9\+._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value); 
	}, 
	"Please enter a valid email address."
	);
	///^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i
	$("#new_student").validate({

		errorClass: 'invalid_input_field',

		rules: {   
			'student[email]' : {
									required: true, 
									email: true,
									remote: {
							        	url: 'check_email_availability',
							        	//type: "get",
							        	data: {
								         	email: function() {
								            	return $('#student_email').val();
								          	}
								        }
							       	}
								},

			'student[first_name]' : {
										required: true, 
										minlength: 3
									},

			'student[percentage]' : {
										required: true, 
										number: true,
										min: 30,
										max: 100
									},

			'student[college_id]' : {
										required: true,
										digits: true
									},

			'student[course_id]' : {
										required: true,
										digits: true
									},

			'student[branch_id]' : {
										required: true,
										digits: true
									},

			'student[year_of_passing]' : {
										  	required: true,
										  	digits: true,
										  	max: 2030,
										  	min: 2014
										  },

			'student[password]' : {
									required: true,
									rangelength: [6,12]
								},
		},
		messages: {
			'student[email]' : {remote: "Email ID already taken."},

			'student[college_id]': "Please select your college from the list.",

			'student[course_id]': "Please select your course from the list.",

			'student[branch_id]': "Please select your branch from the list.",

			'student[year_of_passing]': "Please select the year you're graduated/ing in.",
		},

		tooltip_options: {
			'_all_': {trigger:'focus'},

			'student[email]': {placement: 'right',html: true},

			'student[first_name]': {placement: 'left',html: true},

			'student[college_id]': {placement: 'right',html: true},

			'student[course_id]': {placement: 'left',html: true},

			'student[branch_id]': {placement: 'right',html: true},

			'student[percentage]': {placement: 'left',html: true},

			'student[year_of_passing]': {placement: 'right',html: true},

			'student[password]': {placement: 'right',html: true}
		},

		invalidHandler: function(form, validator) {

		}
	});

$("#new_tpo").validate({

		errorClass: 'invalid_input_field',

		rules: {   
			'tpo[email]' : {
								required: true, 
								email: true,
								remote: {
						        	url: 'check_email_availability',
						        	//type: "get",
						        	data: {
							         	email: function() {
							            	return $('#tpo_email').val();
							          	}
							        }
							    }
							},

			'tpo[first_name]' : {
									required: true, 
									minlength: 3
								},

			'tpo[college]' : {
								required: true,
								remote: {
						        	url: 'check_college_name_availability',
						        	//type: "get",
						        	data: {
							         	college: function() {
							            	return $( "#tpo_college" ).val();
							          	}
							        }
							    }
							  },

			'tpo[location_id]' : {
									required: true,
									digits: true
								},

			'tpo[phone_no]' : {
									required: true,
									digits: true,
									rangelength : [8,12]
								},

			'tpo[password]' : {
								required: true,
								minlength: 6,
								maxlength: 12
							  }
		},
		messages: {
			'tpo[email]' : {remote: "Email ID already taken."},

			'tpo[college]' : {remote: "College name already taken. Try entering full name of your college with location"}
		},

		tooltip_options: {
			
			'tpo[email]': {placement: 'right',html: true},

			'tpo[first_name]': {placement: 'left',html: true},

			'tpo[college]': {placement: 'left',html: true},

			'tpo[password]': {placement: 'right',html: true}

		},

		invalidHandler: function(form, validator) {
			$(document).scrollTop(0);
		}
	});

	$('.forgot_password_form_container form').validate({

		errorClass : 'invalid_input_field',

		rules : {
				'password_reset[email]' : {
											required : true,
											email : true,
										  }
		} ,
		messages : {
			
		},

		tooltip_options : {
							'password_reset[email]' : {placement : 'right'}
						}
	});
});
