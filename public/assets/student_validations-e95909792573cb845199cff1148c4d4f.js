jQuery.validator.addMethod("greaterThan", 
function(value, element, params) {

    if (!/Invalid|NaN/.test(new Date(value))) {
        return new Date(value) > new Date($(params).val());
    }

    return isNaN(value) && isNaN($(params).val()) 
        || (Number(value) > Number($(params).val())); 
},'Must be greater than {0}.');

$('.account_settings').validate({

	errorClass : 'invalid_input_field',

	rules : {
		'student[password]' : {rangelength : [6,12]}
	},

	tooltip_options : {
		'student[password]' : {placement : 'right'}
	}
});

function validate_general_info(){
	$('.edit_student').validate({

		errorClass : 'invalid_input_field',

		rules : {
			'student[first_name]' : {
										required : true,
										minlength : 3,
									},

			'student[phone_no]' : {
									digits : true,
									rangelength : [8,10]
								   },

			'student[location_id]' : {
										digits : true,
										required : true
									  },

			'student[college_id]' : {
										required : true,
										digits : true
									},

			'student[course_id]' : {
										required : true,
										digits : true
									},

			'student[branch_id]' : {
										required : true,
										digits : true
									},

			'student[percentage]' : {
										required : true,
										number : true
									},

			'student[year_of_passing]' : {
										required : true,
										digits : true
									},

			'student[social_profile_attributes][website]' : {
																url : true
															},

			'student[social_profile_attributes][linkedin]' : {
																url : true
															},

			'student[social_profile_attributes][github]' : {
																url : true
															},

			'student[image]' : {
									accept: "image/*"
								}

		},

		messages : {
			'student[first_name]' : '',

			'student[location_id]' : "Please select your location.",

			'student[course_id]' : "Please select your course.",

			'student[branch_id]' : "Please select your branch.",

			'student[year_of_passing]' : "Please select the year in which you are graduating.",

			'student[social_profile_attributes][website]' : "Make Sure URL starts with http",

			'student[social_profile_attributes][linkedin]' : "Make Sure URL starts with http",

			'student[social_profile_attributes][github]' : "Make Sure URL starts with http",

			'student[image]' : "Invalid Format"

		},

		tooltip_options : {
			'_all_' : {trigger:'keyup'},

			'student[year_of_passing]' : {placement : 'right'},

			'student[social_profile_attributes][website]' : {placement : 'top'},

			'student[social_profile_attributes][linkedin]' : {placement : 'right'},

			'student[social_profile_attributes][github]' : {placement : 'bottom'},

			'student[image]' : {placement: 'top', html: true}
		},

		invalidHandler: function(form, validator) {
			$(document).scrollTop(0);
			$('#form_error_message').removeClass('hidden');
		}

	});

	validate_education();
	validate_experience();
	validate_project();
}

function validate_education(){
	$('table.prev_education input.institute').each(function(){
		$(this).rules("add", { 
	      required : true,
	      messages: {
    		required: "",
    	  }
	    });
	});

    $('table.prev_education input.degree').each(function(){
		$(this).rules("add", { 
	      required : true,
	      messages: {
    		required: "",
    	  }
	    });
	});

	$('table.prev_education input.percentage').each(function(){
		$(this).rules("add", { 
	      digits : true,
	      range : [30, 100]
	    });
	});

	$('table.prev_education input.year_of_passing').each(function(){
		$(this).rules("add", { 
	      digits: true,
	      messages: {
    		required: "",
    	  }
	    });
	});

}

function validate_experience(){
	$('.experience_wrapper .experience input.company_name').each(function(){
		$(this).rules("add", { 
	      required : true
	    });
	});

	$('.experience_wrapper .experience input.position').each(function(){
		$(this).rules("add", { 
	      required : true,
	      messages: {
    		required: "",
    	  }
	    });
	});

	$('.experience_wrapper .experience input.date').each(function(){
		$(this).rules("add", { 
	      required : true,
	      date : true,
	      messages: {
    		required: "",
    		date: ""
    	  }
	    });
	});
}

function validate_project(){
	$('.project_wrapper .project input.project_name').each(function(){
		$(this).rules("add", { 
	      required : true,
	      messages: {
    		required: "",
    	  }
	    });
	});
}
;
