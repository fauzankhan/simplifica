# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/account_activation
  def account_activation
    user = Tpo.first
    user.activation_token = Tpo.new_token
    UserMailer.account_activation(user)
  end

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/password_reset
  def password_reset
    user = Tpo.first
    user.reset_token = Tpo.new_token
    UserMailer.password_reset(user)
  end

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/new_college_registered
  def new_college_registered
    user = Tpo.last
    user.reset_token = Tpo.new_token
    UserMailer.new_college_registered(user)
  end

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/tpo_welcome_message
  def tpo_welcome_message
    user = Tpo.first
    UserMailer.tpo_welcome_message(user)
  end

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/tpo_activation
  def tpo_activation
    tpo = Tpo.first
    UserMailer.tpo_activation(tpo)
  end

end
