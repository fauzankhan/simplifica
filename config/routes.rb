Rails.application.routes.draw do

  root 'welcome#index'
  get 'welcome/index'
  get 'password_resets/new'
  get 'password_resets/edit'
  get '/about' => 'welcome#about', as: :about
  get '/login' => 'sessions#new', as: :new_session
  get 'students/populate_branches'
  get 'students/:id/populate_branches' => 'students#populate_branches' 
  get 'jobposts/populate_branches'
  get 'jobposts/:id/populate_branches' => 'jobposts#populate_branches' 
  get 'jobposts/:id/view_applicants' => 'jobposts#view_applicants', as: :view_applicants_jobpost
  get 'jobposts/:id/download_excelsheet' => 'jobposts#download_excelsheet', as: :download_excelsheet_jobpost
  get '/populate_branches'  => 'jobposts#populate_branches'
  get 'jobposts/shortlist_applicant' => 'jobposts#shortlist_applicant'
  get 'jobposts/select_applicant' => 'jobposts#select_applicant'
  get 'students/:id/account_settings' => 'students#account_settings', as: :account_settings_student
  get 'students/:id/my_applications' => 'students#my_applications', as: :my_applications_student
  patch 'students/:id/account_settings' => 'students#update_account'
  get '/students/check_email_availability' => 'all_users#check_email_availability'
  get '/tpos/check_email_availability' => 'all_users#check_email_availability'
  get '/password_resets/check_email_availability' => 'all_users#check_email_availability'
  get '/tpos/check_college_name_availability' => 'tpos#check_college_name_availability'
  get '/tpos/:id/check_email_availability' => 'all_users#check_email_availability'
  get '/tpos/:id/check_college_name_availability' => 'tpos#check_college_name_availability'
  patch '/jobposts/:id/:tpo_note' => 'jobposts#tpo_note'

  resources :tpos
  resources :students
  resources :welcome
  resources :sessions
  resources :all_users
  resources :jobposts
  resources :locations
  resources :courses
  resources :account_activations, only: [:new, :create, :edit]
  resources :password_resets, only: [:edit, :new, :create, :update]

  delete 'logout' => 'sessions#destroy'
  post 'job_applications' => 'job_applications#create', as: :submit_application
  get '/job_feed' => 'students#job_feed', as: :jobfeed_student
  get '/student_search' => 'tpos#search_students', as: :search_students_tpo
  get '/admin_panel' => 'tpos#admin_panel', as: :admin_panel
  get 'jobposts/:id/print_applicants' => 'jobposts#print_applicants', as: :print_applicants_jobpost
  post '/tpos/activate_tpo' => 'tpos#activate_tpo', as: :activate_tpo

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
